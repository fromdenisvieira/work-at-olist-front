![Logo of the project](./src/assets/images/logotype.svg)

# olist Sign Up &middot; [![Build Status](https://img.shields.io/travis/npm/npm/latest.svg?style=flat-square)](https://travis-ci.org/npm/npm) [![npm](https://img.shields.io/npm/v/npm.svg?style=flat-square)](https://www.npmjs.com/package/npm) [![PRs Welcome](https://img.shields.io/badge/PRs-welcome-brightgreen.svg?style=flat-square)](http://makeapullrequest.com) [![GitHub license](https://img.shields.io/badge/license-MIT-blue.svg?style=flat-square)](https://github.com/your/your-project/blob/master/LICENSE)
> javascript

A brief description of your project, what it is used for.

## Installing / Getting started

A quick introduction of the minimal setup you need to get a hello world up &
running.

```shell
commands here
```

Here you should say what actually happens when you execute the code above.

## Developing

### Built With

- Javascript
- [ParcelJS](https://parceljs.org/)
- [Sass](https://sass-lang.com/)
- [RSCSS](http://rscss.io/)
- [Atomic Design Pattern](http://bradfrost.com/blog/post/atomic-web-design/)

### Prerequisites
What is needed to set up the dev environment. For instance, global dependencies or any other tools. include download links.

- node
- yarn or npm 
- [parceljs](https://parceljs.org/getting_started.html)

### Setting up Dev

Here's a brief intro about what a developer must do in order to start developing
the project further:

```shell
git clone https://gitlab.com/fromdenisvieira/work-at-olist-front.git
cd your-project/
yarn
yarn start
```

And state what happens step-by-step. If there is any virtual environment, local server or database feeder needed, explain here.

### Building

If your project needs some additional steps for the developer to build the
project after some code changes, state them here. for example:

```shell
yarn build
```

Here again you should state what actually happens when the code above gets
executed.

### Deploying / Publishing
give instructions on how to build and release a new version
In case there's some step you have to take that publishes this project to a
server, this is the right time to state it.

```shell
packagemanager deploy your-project -s server.com -u username -p password
```

And again you'd need to tell what the previous code actually does.

## Versioning

We can maybe use [SemVer](http://semver.org/) for versioning. For the versions available, see the [link to tags on this repository](https://gitlab.com/fromdenisvieira/work-at-olist-front/tags).


## Configuration

Here you should write what are all of the configurations a user can enter when
using the project.


## Licensing

State what the license is and how to find the text version of the license.